$ ( "#botao-placar" ).click ( MostraPlacar );
$ ( "#botao-sync" ).click ( SincronizaPlacar );

//deletar linha
function RemoveLinha ( ) {
    event.preventDefault ( );
    var linha = $ ( this ).parent ( ).parent ( );
    //fadeOut nao funciona, mas excluir a linha normalmente
    linha.fadeOut ( 500 );
    setTimeout ( function ( ) {
      linha.remove ( );
    }, 1000 )
}

//inserir pontos no Placar
function InserePlacar ( ) {
  var corpoTabela =  $ ( ".placar" ).find ( "table" );
  var usuario = $ ( "#usuarios" ).val ( );
  var numPalavras = $ ( "#contador-palavras" ).text ( );

  var linha = NovaLinha ( usuario, numPalavras );

  linha.find ( ".botao-remover" ).click ( RemoveLinha );

  corpoTabela.prepend ( linha );

  $ ( ".placar" ).slideDown ( 500 );
  ScrollPlacar ( );
}

//criar linha tabela
function NovaLinha ( _usuario, _palavras ) {
  var linha = $ ( "<tr>" );
  var colunaUsuario = $ ( "<td>" ).text ( _usuario );
  var colunaPalavras = $ ( "<td>" ).text ( _palavras );
  var colunaRemover = $ ( "<td>" );
  var link = $ ( "<a>" ).addClass ( "botao-remover" ).attr ( "href", "#" );
  var icone = $ ( "<i>" ).addClass ( "small" ).addClass ( "material-icons" ).text ( "delete" );

  link.append ( icone );
  colunaRemover.append ( link );

  linha.append ( colunaUsuario );
  linha.append ( colunaPalavras );
  linha.append ( colunaRemover );

  return linha;
}

//mostrar placar
function MostraPlacar ( ) {
  $ ( ".placar" ).stop ( ).slideToggle ( 600 );
}

//scroll pagina
function ScrollPlacar ( ) {
  var posicaoPlacar = $ ( ".placar" ).offset ( ).top;

  $ ( "body" ).animate ( { scrollTop: posicaoPlacar + "px" }, 1000 );
}

//sincronizar placar no servidor
function SincronizaPlacar ( ) {
  var placar = [ ];
  var linha = $ ( "tbody > tr" );
  linha.each ( function ( ) {
    var usuario = $ ( this ).find ( "td:nth-child(1)" ).text ( );
    var palavras = $ ( this ).find ( "td:nth-child(2)" ).text ( );

    var score = { usuario: usuario, pontos: palavras };
    placar.push ( score );
  } );

  var dados = {
    placar: placar
  };

  $.post ( "http://localhost:3000/placar", dados, GravouDados ).fail ( ErroRequisicaoPost );
}

//atualiza placar quando entra na pagina
function AtualiaPlacar ( ) {
  $.get ( "http://localhost:3000/placar", PreencherTabela );
}

//preencher Placar
function PreencherTabela ( data ) {
  $ ( data ).each ( function ( ) {
    var linha = NovaLinha ( this.usuario, this.pontos );
    linha.find ( ".botao-remover" ).click ( RemoveLinha );
    $ ( "tbody" ).append ( linha );
  } )
}

//sucesso em gravar os Dados
function GravouDados ( ) {
  $ ( ".tooltip-trigger" ).tooltipster ( "open" ).tooltipster ( "content", "Sucesso ao sincronizar" );
  $ ( "#sync" ).toggle ( );
  setTimeout ( function ( ) {
    $ ( "#sync" ).toggle ( );
    $ ( ".tooltip-trigger" ).tooltipster ( "close" );
  }, 3000 )
}

//mensagem de erro para gravar os dados
function ErroRequisicaoPost ( ) {
  $ ( ".tooltip-trigger" ).tooltipster ( "open" ).tooltipster ( "content", "Falha ao sincronizar" );
  $ ( "#sync-erro" ).toggle ( );
  setTimeout ( function ( ) {
    $ ( "#sync-erro" ).toggle ( );
    $ ( ".tooltip-trigger" ).tooltipster ( "close" );
  }, 3000 )
}
