var tempoInicial = $ ( "#tempo-digitacao" ).text ( );
$ ( "#botao-frase" ).click ( FraseAleatoria );
$ ( "#botao-frase-id" ).click ( BuscaFrase );

//tamanho da frase
function AtualizaTamanhofrase ( ) {
  var frase = $ ( ".frase" ).text  ( );
  var numeroPalavras = frase.split ( " " ).length;
  var tamanhoFrase = $ ( "#tamanho-frase" );

  tamanhoFrase.text ( numeroPalavras );
}

//atualizar o tempo de digitação
function AtualizaTempoDigitacao ( tempo ) {
  tempoInicial = tempo;
  $ ( "#tempo-digitacao" ).text ( tempo );
}

//verificador da frase
function VerificadorBorda ( ) {
  campo.on ( "input", function ( ) {
    var frase = $ ( ".frase" ).text ( );
    var digitado = campo.val ( );
    var comparavel = frase.substr ( 0, digitado.length );
    if ( digitado == comparavel ) {
      campo.addClass ( "borda-verde" );
      campo.removeClass ( "borda-vermelha" );
    } else {
      campo.addClass ( "borda-vermelha" );
      campo.removeClass ( "borda-verde" );
    }
  } )
}

//contadores
function InicializaContadores ( ) {
  campo.on ( "input", function ( ) {
    var conteudo = campo.val ( );
    var qtdPalavras = conteudo.split ( /\S+/ ).length - 1;
    $ ( "#contador-palavras" ).text ( qtdPalavras );
    $ ( "#contador-caracteres" ).text ( conteudo.length );
   } )
}

//mudar frase
function FraseAleatoria ( ) {
  $ ( "#spinner" ).toggle ( );

  $.get ( "http://localhost:3000/frases", TrocaFraseAleatoria ). fail ( ErroRequisicao ).always ( function ( ) {
    $ ( "#spinner" ).toggle ( );
    Reset ( );
    InicializaCronometro ( );
  } );
}

//buscar frase pelo id
function BuscaFrase ( ) {
  var fraseID = $ ( "#frase-id" ).val ( );
  var dados = { id: fraseID };

  $ ( "#spinner" ).toggle ( );

  $.get ( "http://localhost:3000/frases", dados, TrocaFrasePeloID ). fail ( ErroRequisicao ).always ( function ( ) {
    $ ( "#spinner" ).toggle ( );
    Reset ( );
    InicializaCronometro ( );
  } );
}

//trocar frase aleatoria
function TrocaFraseAleatoria ( data ) {
  var frase = $ ( ".frase" );
  var dataLocal = data [ Math.floor ( Math.random ( ) * data.length ) ];
  frase.text ( dataLocal.texto );
  AtualizaTamanhofrase ( );
  AtualizaTempoDigitacao ( dataLocal.tempo );
}

//trocar frase pelo id
function TrocaFrasePeloID ( data ) {
  var frase = $ ( ".frase" );
  frase.text ( data.texto );
  AtualizaTamanhofrase ( );
  AtualizaTempoDigitacao ( data.tempo );
}

//function fail
function ErroRequisicao ( ) {
  $ ( "#erro" ).toggle ( );
  setTimeout ( function ( ) {
    $ ( "#erro" ).toggle ( );
  }, 3000 )
}
