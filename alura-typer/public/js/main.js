
var campo = $ ( ".campo-digitacao" );

$ ( function ( ) {
  AtualizaTamanhofrase ( );
  InicializaContadores ( );
  InicializaCronometro ( );
  VerificadorBorda ( );
  $ ( "#botao-reiniciar" ).click ( ButtonReiniciar );
  AtualiaPlacar ( );

  $ ( "#usuarios" ).selectize ( {
    create: true,
    sortField: 'text'
  } );


  $ ( ".tooltip-trigger" ).tooltipster ( {
    trigger: 'custom'
  } );

  $ ( ".tooltip" ).tooltipster ( );

} )

//cronometro
function InicializaCronometro ( ) {
  campo.one ( "focus", function ( ) {
    var temporestante = $ ( "#tempo-digitacao" ).text ( );
    var cronometroID = setInterval ( function ( ) {
        temporestante--;
        $ ( "#tempo-digitacao" ).text ( temporestante );
        if ( temporestante <= 0 ) {
          clearInterval ( cronometroID );
          Finalizargame ( );
        }
    }, 1000 );
  } )
}

//button reiniciar
function ButtonReiniciar ( ) {
  if ( $ ( "#tempo-digitacao" ).text ( ) <= 0 )
  {
    Reset ( );
    InicializaCronometro ( );
  }
}

//resetar informações
function Reset ( ) {
  campo.attr ( "disabled", false );
  campo.val ( "" );
  $ ( "#contador-palavras" ).text ( "0" );
  $ ( "#contador-caracteres" ).text ( "0" );
  $ ( "#tempo-digitacao" ).text ( tempoInicial );
  campo.toggleClass ( "campo-desativado" );

  campo.removeClass ( "borda-verde" );
  campo.removeClass ( "borda-vermelha" );
}

//finaliza jogo
function Finalizargame ( ) {
  campo.attr ( "disabled", true );
  campo.toggleClass ( "campo-desativado" );
  InserePlacar ( );
}
